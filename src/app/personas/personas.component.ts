import { Component, OnInit } from '@angular/core';
import { Persona } from './persona';
import { PersonaService } from './persona.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  titulo: String = "Lista de Personas.";

  personas:Persona[];
  lista:string[]=["hola","que","tal","estas"];

  constructor(private personaServices:PersonaService) { }

  ngOnInit(): void {
    this.personaServices.getAll().subscribe(data => this.personas = data);
  }

}
