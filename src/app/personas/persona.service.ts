import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Persona } from './persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  constructor(private http: HttpClient) { }

  //Obtengo la lista de las personas registradas.
  getAll(): Observable<Persona[]> {
    return this.http.get<Persona[]>('http://localhost:8080/api/personas');
  }

  //Obtengo la persona por cedula.
  getById(id: String): Observable<Persona> {
    return this.http.get<Persona>('http://localhost:8080/api/persona/' + id);
  }

  //Creamos un registro de la Persona
  create(persona: Persona): Observable<Persona> {
    return this.http.post<Persona>('http://localhost:8080/api/persona', persona);
  }

}
