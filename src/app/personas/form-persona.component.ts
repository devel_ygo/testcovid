import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from './persona';
import { PersonaService } from './persona.service';

@Component({
  selector: 'app-form-persona',
  templateUrl: './form-persona.component.html',
  styleUrls: ['./form-persona.component.css']
})
export class FormPersonaComponent implements OnInit {

  title:String = "Registro de Perona";
  persona:Persona = new Persona();
  messageErro:String;
  constructor(private personaSerice:PersonaService, private router:Router) { }

  ngOnInit(): void {
  }

  create():void{
    console.log(this.persona);
    this.personaSerice.create(this.persona).subscribe(
      res => {this.router.navigate(['/personas'])},
      err => {this.messageErro = "ERROR-400"}
    );
  }

}
